import files.FileGrabber;
import org.apache.commons.io.FilenameUtils;
import utils.RelativeReplace;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Roxor GDK Offline Translator");
        //initial root location
        File rootLocation;
        String rootFolderName;
        String urlToRelative = "";
        if (args.length > 0) {
            rootLocation = new File(args[0]);
            urlToRelative = args[1];
            String[] levels = rootLocation.getAbsolutePath().split(Pattern.quote(File.separator));
            rootFolderName = levels[levels.length - 1];
        } else {//TODO: local test, replace my user path with yours!!!
            rootLocation = new File("/Users/cosmin.oance/Documents/GIT/roxor-gdk/website/build/roxor-gdk-test");
            urlToRelative = "https://roxorgaming.gitlab.io/games-studios/common/gdk/roxor-gdk/";
            String[] levels = rootLocation.getAbsolutePath().split(Pattern.quote(File.separator));
            rootFolderName = levels[levels.length - 1];
        }
        FileGrabber grabber = new FileGrabber(rootLocation, "html");
        ArrayList<File> files = grabber.getFiles();
        for (File f : files
        ) {
            String content = "";
            try {
                BufferedReader reader =
                        new BufferedReader(new FileReader(f));
                String str;
                while ((str = reader.readLine()) != null) {
                    content += str;
                }
                reader.close();
            } catch (
                    IOException e) {
                throw e;
            }
            RelativeReplace replacementPattern = new RelativeReplace(urlToRelative, content, rootFolderName);
            content = replacementPattern.makeRelative(f.getAbsolutePath(), true, "/index.html");
            String oldPath = f.getAbsolutePath();
            writeToFile(f, content, oldPath);

        }
    }

    private static void writeToFile(File f, String content, String oldPath) throws IOException {
        File output = new File(oldPath.replace(f.getName(),
                FilenameUtils.getBaseName(oldPath) + "."
                        + FilenameUtils.getExtension(oldPath)));
        output.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(output));
        writer.write(content);
        writer.close();
        System.out.println("wrote result to " + output.getAbsolutePath());
    }
}
