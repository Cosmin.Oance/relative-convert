package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexReplace {
    public String pattern;
    public String replacement;

    public RegexReplace(String pattern, String replacement) {
        this.pattern = pattern;
        this.replacement = replacement;
    }

    public static String regExReplace(String text, String pattern, String replacement) {
        Pattern p = Pattern.compile(pattern);
        Matcher matches = p.matcher(text);
        int nrOfMatches = 0;
        while (matches.find()) {
            nrOfMatches++;
            String replace = matches.group(1);
            if(!replace.endsWith(replacement) && !replace.endsWith(".html") && !replace.endsWith(".pdf")) {
                String separator = "";
                if(!replace.endsWith("/")){
                    separator = "/";
                }
                String toReplaceWith = replace + separator + replacement;
                text = text.replaceFirst(replace, toReplaceWith);
            }

        }
        //this is how desperate I am
        while(text.contains("/index.html/index.html")){
            text = text.replace("/index.html/index.html","/index.html");
        }


        return text;
    }

    public static String regExReplace(String text, RegexReplace pattern) {
        return regExReplace(text,pattern.pattern,pattern.replacement);
    }
}
