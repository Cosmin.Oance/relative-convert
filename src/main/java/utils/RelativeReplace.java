package utils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RelativeReplace {
    private String url;
    private String text;
    private String rootFolder;

    public RelativeReplace(String url, String text, String rootFolder) {
        this.url = url;
        this.text = text;
        this.rootFolder = rootFolder;
    }

    public String makeRelative(String filePath, Boolean addSuffix, String... suffix) {
        String suffixValue = addSuffix ? suffix[0] : "";
        String newText = this.text;
        int index = filePath.indexOf(rootFolder);
        long level = filePath.substring(index, filePath.length()).chars().filter(sep -> sep == File.separator.toCharArray()[0]).count() - 1;
        String rootPrefix = "";
        for (int i = 0; i < level; i++) {
            rootPrefix += "../";
        }
        Pattern p = Pattern.compile("(?:href|src)=\"(" + Pattern.quote(url) + ".*?)\"");
        Matcher matches = p.matcher(newText);
        while (matches.find()) {
            String replace = matches.group(1);
            String shortPath = replace.replace(url, "");
            String toReplaceWith = rootPrefix + shortPath;
            //take anchors into consideration
            String anchor = "";
            if (toReplaceWith.contains("#")) {
                String[] urlWithAnchor = toReplaceWith.split("#");
                if(urlWithAnchor.length>1) {
                    toReplaceWith = urlWithAnchor[0];
                    anchor = "#" + urlWithAnchor[1];
                }
                else if (toReplaceWith.endsWith("#")){
                  toReplaceWith = toReplaceWith.substring(0,toReplaceWith.length()-1);
                }
            }
            //add index.html to <folder-name> root files
            if (!toReplaceWith.endsWith(".html") && !toReplaceWith.endsWith(".pdf")
                    && !toReplaceWith.endsWith(".js")
                    && !toReplaceWith.endsWith(".gif")
                    && !toReplaceWith.endsWith(".css")
                    && !toReplaceWith.endsWith(".ico")
                    && !toReplaceWith.endsWith(".png")
                    && !toReplaceWith.endsWith(".jpg")
                    && !toReplaceWith.endsWith(".jpeg")
            ) {
                String finalSuffix = suffixValue;
                if (shortPath.isEmpty()) {
                    finalSuffix = "index.html";
                }
                if(toReplaceWith.endsWith("/")||toReplaceWith.isEmpty()){
                    finalSuffix = "index.html";
                }
                // add anchor (can be empty)
                toReplaceWith += finalSuffix + anchor;
            }
            try {
                newText = new StringBuilder(newText).replace(matches.start(1), matches.end(1), toReplaceWith).toString();
                matches = p.matcher(newText);
            } catch (Exception e) {
                System.out.println(filePath);
                throw e;
            }
        }

        return newText;
    }
}
