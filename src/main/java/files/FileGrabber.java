package files;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;

public class FileGrabber {
    ArrayList<File> files;
    File location;
    String[] extension;

    public FileGrabber(File location, String extension) {
        this.location = location;
        if(extension.equals("")) {
            this.extension = null;
        }
        else{
            this.extension = new String[]{extension};
        }
    }

    public ArrayList<File> getFiles() {
        this.files = new ArrayList<File>(FileUtils.listFiles(location, this.extension, true));
        return this.files;
    }
}
