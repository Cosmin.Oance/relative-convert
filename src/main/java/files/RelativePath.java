package files;

import org.apache.commons.io.FilenameUtils;
import utils.RegexReplace;

import java.io.*;

public class RelativePath {
    RegexReplace pattern;
    String toReplaceWith;

    public RelativePath(RegexReplace pattern, String replaceWith) {
        this.pattern = pattern;
        this.toReplaceWith = replaceWith;
    }

    public File processFile(File file) throws IOException {
        String content = "";
        try {
            BufferedReader reader =
                    new BufferedReader(new FileReader(file));
            String str;
            while ((str = reader.readLine()) != null) {
                content += str;
            }
            reader.close();
        } catch (
                IOException e) {
            throw e;
        }
        content = RegexReplace.regExReplace(content, pattern);

        String oldPath = file.getAbsolutePath();
        File output = new File(oldPath.replace(file.getName(),
                FilenameUtils.getBaseName(oldPath)+"."
                        + FilenameUtils.getExtension(oldPath)));
        output.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(output));
        writer.write(content);
        writer.close();
        System.out.println("wrote result to " + output.getAbsolutePath());
        return output;
    }
}
