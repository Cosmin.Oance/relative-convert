import files.RelativePath;
import org.testng.annotations.Test;
import utils.RegexReplace;

import java.io.File;
import java.io.IOException;

public class RelativePathTest {
    @Test
    public void testGrabber() throws IOException {
        File f = new File(getClass().getClassLoader().getResource("test.html").getFile());
        RegexReplace replace = new RegexReplace("href=\\\"([^\\.\\\"]*?)\\\"", "/index.html");
        RelativePath proc = new RelativePath(replace, "");
        proc.processFile(f);
    }
}
