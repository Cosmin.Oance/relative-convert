import org.testng.Assert;
import org.testng.annotations.Test;
import utils.RegexReplace;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReplaceTest {

    @Test
    public void testGrabber() throws IOException {
        String content = "";
        String toReplace = "https://github.gamesys.co.uk/pages/game-studios-core/roxor-gdk/";
        String replaceWith = "";
        try {
            BufferedReader in =
                    new BufferedReader(new FileReader(getClass().getClassLoader().getResource("test.html").getFile()));
            String str;
            while ((str = in.readLine()) != null) {
                str = str.replace(toReplace,replaceWith);
                String replacement = RegexReplace.regExReplace(str,"href=\\\"([^\\.\\\"]*?)\\\"","/index.html");
                if(!replacement.isEmpty()){
                    System.out.println(str + "needs replacing with " + replacement);
                    str = replacement;
                }
                content +=str;
            }
            in.close();
        } catch (IOException e) {
            throw e;
        }
        System.out.println(content);
        Assert.assertFalse(content.contains(toReplace));
    }

}
