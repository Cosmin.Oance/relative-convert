import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;

public class FileGrab {
    ArrayList<File> list = new ArrayList<File>();
    @BeforeTest
    public void  setup(){
        list = new ArrayList<File>(FileUtils.listFiles(new File("."),null,true));
    }

    @Test
    public void testGrabber() {
        for (File f : list
        ) {
            System.out.println(f.getAbsolutePath());
        }
        Assert.assertTrue(list.size()>0);
    }

}
