# relative-convert
# Intro

This project is only currently doing one thing, taking a generated `roxor-gdk` folder and changing all resources to point to its local
 files, instead of absolute URLs.

In order to generate the `roxor-gdk` quasi-local files, you must clone the `roxor-gdk` repo and run the build command.

```npm run build ```

Upon completion, there will be a `website/build/roxor-gdk`. 
This can be converted to fully offline mode.

# Generating JAR
## Requirements
Java 8 is required to build the project.
To generate the runnable JAR, run this `maven` command.


```mvn clean assembly:assembly -DdescriptorId=jar-with-dependencies```


A JAR file will be generated in the _target_ folder of the project `/target/document-offline-translate-1.0-SNAPSHOT-jar-with-dependencies.jar`


# Running the JAR

The JAR takes 3 arguments:
- Location of the `target` folder
- The original base URL at the moment of build (_https://github.gamesys.co.uk/pages/game-studios-core/roxor-gdk_)

Example command to run the conversion:
```
java -jar document-offline-translate-1.0-SNAPSHOT-jar-with-dependencies.jar "pathToRoxorGdk/website/build/roxor-gdk-gh-pages" "https://github.gamesys.co.uk/pages/game-studios-core/roxor-gdk/"
```

- please note that arguments are between single quotes \'

